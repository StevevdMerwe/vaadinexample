package za.co.i1solutions.spring.views;

import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@Route(value = HelloView.ROUTE, layout = MainView.class)
@PageTitle(HelloView.TITLE)
public class HelloView extends HorizontalLayout {
    static final String TITLE = "Playing";
    static final String ROUTE = "hello";

    public HelloView() {
        Tabs tabs = new Tabs(new Tab("Tab one"), new Tab("Tab two"),
            new Tab("Tab three") );
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.setHeight("130px");
        add(tabs ,
            new H4("Welcome Steve !"));
    }
}

