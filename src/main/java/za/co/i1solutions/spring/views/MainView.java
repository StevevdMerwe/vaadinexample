package za.co.i1solutions.spring.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Shortcuts;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.AppLayoutMenu;
import com.vaadin.flow.component.applayout.AppLayoutMenuItem;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import java.net.InetAddress;
import java.net.UnknownHostException;


@Route
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
@Theme(value = Lumo.class, variant = Lumo.DARK)
@HtmlImport("styles.html")
public class MainView extends AppLayout implements RouterLayout, AfterNavigationObserver {

    private FlexLayout childWrapper = new FlexLayout();
    private AppLayoutMenu menu = createMenu();

    public MainView() {
        Image img = new Image("http://www.i1solutions.co.za/wp-content/uploads/2018/11/i1-Solutions-logo.png", "i1 Logo");
        img.setHeight("35px");
        setBranding(img);
        getElement().getStyle().set("--vaadin-app-layout-navbar-background", "var(--lumo-tint-30pct)");


        menu.addMenuItems(
            new AppLayoutMenuItem(AccordionView.TITLE, AccordionView.ROUTE),
            new AppLayoutMenuItem(GridView.TITLE, GridView.ROUTE),
            new AppLayoutMenuItem(HelloView.TITLE, HelloView.ROUTE)
        );

        childWrapper.setSizeFull();
        setContent(childWrapper);

        Shortcuts.addShortcutListener(this, () -> getUI().ifPresent(ui -> ui.navigate(AccordionView.ROUTE)), Key.F1);
        Shortcuts.addShortcutListener(this, () -> getUI().ifPresent(ui -> ui.navigate(GridView.ROUTE)), Key.F2);
        Shortcuts.addShortcutListener(this, () -> getUI().ifPresent(ui -> ui.navigate(HelloView.ROUTE)), Key.F3);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        try {
            getUI().get().getSession().setAttribute("hostAddress", InetAddress.getLocalHost().getHostAddress().toString());
            System.out.println(InetAddress.getByName(InetAddress.getLocalHost().getHostAddress()).getHostName());
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        childWrapper.getElement().appendChild(content.getElement());
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        menu.getMenuItemTargetingRoute(event.getLocation().getPath()).ifPresent(menuItem -> menu.selectMenuItem(menuItem));
    }
}


