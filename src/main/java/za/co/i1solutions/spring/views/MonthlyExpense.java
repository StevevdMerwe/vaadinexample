package za.co.i1solutions.spring.views;

import lombok.Data;

@Data
class MonthlyExpense {
    private String month;
    private Double expenses;
    private int year;

    MonthlyExpense(String month, int year, Double expenses) {
        this.month = month;
        this.expenses = expenses;
        this.year = year;
    }
}
